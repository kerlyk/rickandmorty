package com.kerly.therickandmorty

import com.google.gson.annotations.SerializedName

data class CharacterResponse (
    @SerializedName("id") var id:Int = 0,
    @SerializedName("name") var name:String = "",
    @SerializedName("status") var status:String = "",
    @SerializedName("species")var species:String = "",
    @SerializedName("type")var type:String = "",
    @SerializedName("gender")var gender:String = "",
    @SerializedName("origin") var origin: Origin = Origin(),
    @SerializedName("location")var location: Location = Location(),
    @SerializedName("image")var image:String = "",
    @SerializedName("episode") var episode:List<String> = listOf(),
    @SerializedName("url")var url:String = "",
    @SerializedName("created")var created:String = ""
){
    data class Location(
        @SerializedName("name") val name: String = "",
        @SerializedName("url") val url: String = ""
    )

    data class Origin(
        @SerializedName("name") val name: String = "",
        @SerializedName("url") val url: String = ""
    )
}

