package com.kerly.therickandmorty

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("character/{character-id}")
    fun getAllCharacters(
        @Path("character-id") characterId:Int
    ): Call<CharacterResponse>

    @GET("character")
    fun getCharactersPage(
        @Query("page") pageIndex:Int
    ): Call<GetCharactersResponse>
}
