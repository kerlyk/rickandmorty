package com.kerly.therickandmorty

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kerly.therickandmorty.databinding.FragmentSecondBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SecondFragment : Fragment() {

    private var _binding: FragmentSecondBinding? = null
    private var adapter:CustomAdapter? = null
    var list: RecyclerView? = null
    var layautManager: RecyclerView.LayoutManager? = null
    var pagesSize = 0

    var mSaveList: ArrayList<SaveData>? = null

    private val binding get() = _binding!!

    companion object {
        var character = ArrayList<Character>()
        var filterChar = ArrayList<Character>()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView(character)

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                filterChar.clear()
                for (i in character.indices){
                    if(character[i].name.toLowerCase().contains(query.toLowerCase())) {
                        filterChar.add(character[i])
                    }
                    initRecyclerView(filterChar)
                }
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.isEmpty()){
                    filterChar.clear()
                    for (i in character.indices){
                        filterChar.add(character[i])
                        initRecyclerView(filterChar)
                    }
                    return true
                }else{
                    filterChar.clear()
                    for (i in character.indices){
                        if(character[i].name.toLowerCase().contains(newText.toLowerCase())) {
                            filterChar.add(character[i])
                            initRecyclerView(filterChar)
                        }else{
                            initRecyclerView(filterChar)
                        }
                    }
                    return true
                }
                return false
            }
        })

        binding.searchView.setOnCloseListener(object : SearchView.OnCloseListener {
            override fun onClose(): Boolean {
                filterChar.clear()
                for (i in character.indices){
                    filterChar.add(character[i])
                    initRecyclerView(filterChar)
                }
                return false
            }
        })

    }

    private fun initRecyclerView (list: ArrayList<Character>){
        if(character.isEmpty()){
            getAllNames(1)
        }else{
            binding.loading.isVisible = false
            binding.imageViewIntro.visibility = View.GONE
            binding.imageViewBack.visibility = View.GONE
            createList()
        }
        adapter = CustomAdapter(list, object:ClickListener{
            override fun onClick(vista: View, index: Int) {
                val bundle = Bundle()
                bundle.putInt("idIndex",list.get(adapter?.getItemId(index)!!.toInt()).id)

                bundle.putString("name",list.get(adapter?.getItemId(index)!!.toInt()).name)
                bundle.putString("status",list.get(adapter?.getItemId(index)!!.toInt()).status)
                bundle.putString("gender",list.get(adapter?.getItemId(index)!!.toInt()).gender)
                bundle.putString("species",list.get(adapter?.getItemId(index)!!.toInt()).species)
                bundle.putString("origin",list.get(adapter?.getItemId(index)!!.toInt()).origin?.get(0)!!.name)
                bundle.putString("location",list.get(adapter?.getItemId(index)!!.toInt()).location?.get(0)!!.name)

                findNavController().navigate(R.id.action_SecondFragment_to_characterDescriptionFragment,bundle)

                binding.searchView.setQuery("",false)
                binding.searchView.clearFocus()
            }
        })
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun createList(){
        list = binding.characterList
        list?.setHasFixedSize(true)
        layautManager = LinearLayoutManager(requireContext())
        list?.layoutManager = layautManager
        adapter?.notifyDataSetChanged()
        list?.adapter = adapter
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://rickandmortyapi.com/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getAllNames(page:Int){
        CoroutineScope(Dispatchers.IO).launch {
            val call = getRetrofit().create(ApiService::class.java)
            call.getCharactersPage(page).enqueue(object:Callback<GetCharactersResponse> {
                override fun onResponse(call: Call<GetCharactersResponse>,response: Response<GetCharactersResponse>) {
                    if(!response.isSuccessful){

                    }else{
                        pagesSize = response.body()?.info!!.pages
                        var charSize = response.body()?.results?.indices
                        for (i in charSize!!) {
                            val body = response.body()?.results?.get(i)

                            var location = ArrayList<Location>()
                            location.add(Location(body!!.location.name,body.location.url))

                            var origin = ArrayList<Origin>()
                            origin.add(Origin(body!!.origin.name,body.origin.url))

                            mSaveList?.add(SaveData(body.name))
                            character.add(
                                Character(
                                    body!!.id,
                                    body.name,
                                    body.status,
                                    body.species,
                                    body.type,
                                    body.gender,
                                    body.image,
                                    body.episode,
                                    body.url,
                                    body.created,
                                    location,
                                    origin
                                )
                            )
                        }
                        nextPage(page)
                    }
                }
                override fun onFailure(call: Call<GetCharactersResponse>, t: Throwable) {
                }
            })
        }
        createList()
    }

    fun nextPage(_nextPage:Int){
        var tempPage = _nextPage +1
        getAllNames(tempPage)
        if(tempPage >= pagesSize){
            binding.loading.isVisible = false
            binding.imageViewIntro.visibility = View.GONE
            binding.imageViewBack.visibility = View.GONE
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}