package com.kerly.therickandmorty

import android.view.View

interface ClickListener {
    fun onClick(vista: View, index:Int)
}