package com.kerly.therickandmorty

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class CustomAdapter(items:ArrayList<Character>, var listener: ClickListener): RecyclerView.Adapter<CustomAdapter.ViewHolder>( )   {

    var items:ArrayList<Character>? = null

    init {
        this.items = items
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell,parent,false)
        val ViewHolder = ViewHolder (view, listener)
        return  ViewHolder
    }

    override fun onBindViewHolder(holder: CustomAdapter.ViewHolder, position: Int) {
        val item = items?.get(position)
        holder.bind(item!!.image)
        holder.name?.text =  item?.name
        holder.status?.text = item?.status + " - " + item?.species

    }

    override fun getItemCount(): Int {
        return items?.count()!!
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    class ViewHolder(view:View, listener: ClickListener): RecyclerView.ViewHolder(view), View.OnClickListener{
        var name:TextView? = null
        var status: TextView? = null
        var image:ImageView? = null
        var listener:ClickListener? = null

        init {
            name = view.findViewById(R.id.tvCharacterName)
            status = view.findViewById(R.id.tvStatus)
            image= view.findViewById(R.id.ivPhoto)
            this.listener = listener
            view.setOnClickListener(this)
        }

        fun bind(_image:String){
            Picasso.get().load(_image).into(image)
        }

        override fun onClick(p0: View?) {
            this.listener?.onClick(p0!!, adapterPosition)
        }
    }
}
