package com.kerly.therickandmorty

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kerly.therickandmorty.databinding.FragmentCharacterDescriptionBinding
import com.squareup.picasso.Picasso


class CharacterDescriptionFragment : Fragment() {
    private var _binding: FragmentCharacterDescriptionBinding? = null
    private val binding get() = _binding!!
    var index = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCharacterDescriptionBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        index = arguments?.getInt("idIndex")!!

        var name = arguments?.getString("name")
        binding.tvCharacterNameDescription.text = name

        var status = arguments?.getString("status")
        var gender= arguments?.getString("gender")
        binding.tvStatusDescription.text = status + " - " + gender

        var species= arguments?.getString("species")
        binding.tvStatusSpecies.text = species

        var location = arguments?.getString("location")
        binding.tvTitleLastLocationContent.text = location

        var origin = arguments?.getString("origin")
        binding.vTitleFirstLocationContent.text = origin

        val image = SecondFragment.character
        Picasso.get().load(image[index-1].image).into(binding.ivPhotoDescription)

    }

}

