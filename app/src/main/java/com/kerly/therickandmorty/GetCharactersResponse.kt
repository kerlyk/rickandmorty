package com.kerly.therickandmorty

class GetCharactersResponse (
    val info: Info = Info(),
    val results: List<CharacterResponse> = emptyList()
){
    data class Info(
        val count:Int = 0,
        val pages:Int = 0,
        val next:String = "",
        val prev:String = ""
    )
}
